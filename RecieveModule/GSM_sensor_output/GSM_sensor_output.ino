#include <SoftwareSerial.h>
SoftwareSerial SerialAT(2, 3); // RX, TX

#include <TinyGsmClient.h>
#include <BlynkSimpleSIM800.h>

#define BLYNK_PRINT Serial
#include <SimpleTimer.h>
#include <OneWire.h>

TinyGsm modem(SerialAT);
OneWire  temperatureSensorFirst(A0);
OneWire  temperatureSensorSecond(A1);
const int PIEZO_PIN = A2; // Piezo output

SimpleTimer timer;

#include <TimeLib.h>
//#include <NTPRTC.h>                      // https://github.com/Rom3oDelta7/NTP-RTC
//#include <WorldTimezones.h>               // https://github.com/Rom3oDelta7/NTP-RTC

//Blynk Virtual pins
#define ACTION_BUTTON  V0                                  
#define TIME_DISPLAY    V1
#define DATE_DISPLAY    V2
#define TEMP_IN         V4
#define TEMP_OUT        V5
#define VIBR_SENS       V6

#include <WidgetRTC.h>
WidgetRTC rtc;
BLYNK_ATTACH_WIDGET(rtc, V5);

//Blynk auth token and gprs cred
char auth[] = "f3f4a744c9b042f3ab6d8d707fcdf4c3";
const char apn[]  = "internet.tele2.lv";
const char user[] = "wap";
const char pass[] = "wap";

float sensVal1;
float sensVal2;

void setup()
{
  // Debug console
  Serial.begin(19200);
  delay(10);
  // Blynk will work through Serial
  // Do not read or write this serial manually in your sketch
  SerialAT.begin(19200);
  delay(3000);
  Blynk.begin(auth,modem,apn, user,pass);
  rtc.begin();
  Blynk.syncAll();
  timer.setInterval(1000L, readSensorValues);

//  Blynk.virtualWrite(TIME_DISPLAY, " ");
//  Blynk.virtualWrite(DATE_DISPLAY, " ");
//  NTP_UTC_Timezone(UTC_MST);
//  NTP_Init();

}
void readSensorValues(){
  //Temperature sensors
  sensVal1 = getTemperature(temperatureSensorFirst);
  sensVal2 = getTemperature(temperatureSensorSecond);

  //Vibration sensor
  int piezoADC = analogRead(PIEZO_PIN);
//  float piezoV = piezoADC / 1023.0 * 5.0;
  float piezoV = piezoADC / 200.0 * 5.0;
  //Real time module
//  String currentTime = String(hour()) + ":" + minute() + ":" + second();  
  
  char currentTime[9];  
  
  sprintf(currentTime, "%02d:%02d:%02d", hour(), minute(), second());

  String currentDate = String(year()) + "." + month() + "." + day();
//  String currentDate = String(day()) + "-" + month() + "-" + year();

  //Send data to Blynk app
  Blynk.virtualWrite(TIME_DISPLAY, currentTime);
  Blynk.virtualWrite(DATE_DISPLAY, currentDate);
  Blynk.virtualWrite(TEMP_IN, sensVal1);
  Blynk.virtualWrite(TEMP_OUT, sensVal2);
  Blynk.virtualWrite(VIBR_SENS, piezoADC); 
}

void loop()
{

  Blynk.run();
  timer.run();

}

float getTemperature(OneWire sensor) {
  byte present = 0;
  byte data[12];
  byte address[8];
  float celsius;

  if ( !sensor.search(address)) {
    sensor.reset_search();
    delay(250);
    return;
  }

  sensor.reset();
  sensor.select(address);
  sensor.write(0x44);

  delay(1000);

  present = sensor.reset();
  sensor.select(address);
  sensor.write(0xBE);

  for (byte i = 0; i < 9; i++) {
    data[i] = sensor.read();
  }

  int16_t rawTemperatureValue = (data[1] << 8) | data[0];
  celsius = (float)rawTemperatureValue / 16.0;
  return (celsius);
}

