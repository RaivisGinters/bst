//Blynk includes
#define BLYNK_PRINT Serial
#include <TinyGsmClient.h>
#include <BlynkSimpleSIM800.h>

//Blynk auth token
const char auth[] = "f3f4a744c9b042f3ab6d8d707fcdf4c3";

//GPRS settings
const char apn[]  = "internet.tele2.lv";
const char user[] = "wap";
const char pass[] = "wap";

#include <SoftwareSerial.h>
SoftwareSerial SerialAT(2, 3); // RX, TX

TinyGsm modem(SerialAT);

//Radio module
#include <SPI.h>
//#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(7, 8);
const byte rxAddr[6] = "00001";


struct dataStruct {
  char text[20] = {0};
  float tempVal1;
  float tempVal2;
  char dateAndTime[30];
} mydata;

void setup() {

  while (!Serial);
  Serial.begin(19200);
  delay(10);

  SerialAT.begin(19200);
  delay(3000);
  Blynk.begin(auth, modem, apn, user, pass);

  radio.begin();
  radio.openReadingPipe(0, rxAddr);
  radio.startListening();
}

void loop() {
  Blynk.run();

  if (radio.available())
  {
    radio.read(&mydata, sizeof(mydata));
    Serial.println(mydata.text);
    Serial.println(mydata.tempVal1);
    Serial.println("sanem kko");
    //        Serial.println(mydata.dateAndTime);

    Blynk.virtualWrite(V4, mydata.tempVal1);
    //        Blynk.virtualWrite(V5,mydata.tempVal2);
    //        Blynk.virtualWrite(V6,mydata.vibro);
    delay(1000);
  }
  delay(3000);

}
