//https://learn.sparkfun.com/tutorials/piezo-vibration-sensor-hookup-guide

const int PIEZO_PIN = A3; // Piezo output

void setup() 
{
  Serial.begin(9600);
}

void loop() 
{
  int piezoADC = analogRead(PIEZO_PIN);
  float piezoV = piezoADC / 1023.0 * 5.0;
  if (piezoV > 0)
    Serial.println("Kaut kas sakustejas");
  
}
