//Temperatūra
//http://www.tweaking4all.com/hardware/arduino/arduino-ds18b20-temperature-sensor/

#include <OneWire.h>

OneWire  temperatureSensorFirst(A1);
OneWire  temperatureSensorSecond(A2);

void setup() {
  Serial.begin(9600);
}

void loop() {
  Serial.print("Temperature1: ");
  Serial.println(getTemperature(temperatureSensorFirst));
  Serial.print("Temperature2: ");
  Serial.println(getTemperature(temperatureSensorSecond));
}

float getTemperature(OneWire sensor) {
  byte present = 0;
  byte data[12];
  byte address[8];
  float celsius;

  if ( !sensor.search(address)) {
    sensor.reset_search();
    delay(250);
    return;
  }

  sensor.reset();
  sensor.select(address);
  sensor.write(0x44);

  delay(1000);

  present = sensor.reset();
  sensor.select(address);    
  sensor.write(0xBE);

  for (byte i = 0; i < 9; i++) {
    data[i] = sensor.read();
  }

  int16_t rawTemperatureValue = (data[1] << 8) | data[0];
  celsius = (float)rawTemperatureValue / 16.0;
  return(celsius);
}
