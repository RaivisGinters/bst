///radio
//https://arduino-info.wikispaces.com/Nrf24L01-2.4GHz-HowTo

//Temperatūra
//http://www.tweaking4all.com/hardware/arduino/arduino-ds18b20-temperature-sensor/

//Laiks
//http://www.instructables.com/id/Real-time-clock-using-DS3231-EASY/step2/Library/

//Vibro
//https://learn.sparkfun.com/tutorials/piezo-vibration-sensor-hookup-guide

#include <OneWire.h> //Temperatūra
#include <SPI.h> 
#include <nRF24L01.h>
#include <RF24.h> //Radio
#include <DS3231.h> //Oscilators
#include <String.h>

RF24 radio(7, 8); //Radio
DS3231  rtc(SDA, SCL); //Real Time Clock
OneWire  temperatureFirst(A1); //Abi temperatūras sensori
OneWire  temperatureSecond(A2);
int PIEZO_PIN = A3; // Piezo output

byte rxAddr[6] = "00001";
int sensorPin1 = A1;
int sensorPin2 = A2;
int sensorValue1 = 0;
int sensorValue2 = 0;
char dateAndTime[20];

struct dataStruct{
  char text[20]={0};
  char dateAndTime[30];
  float tempVal1;
  float tempVal2;
}myData;

void setup()
{
  radio.begin();
  radio.setRetries(15, 15);
  radio.openWritingPipe(rxAddr);

  
  rtc.begin();
  // The following lines can be uncommented to set the date and time
  rtc.setDOW(WEDNESDAY);     // Set Day-of-Week to SUNDAY
  rtc.setTime(12, 0, 0);     // Set the time to 12:00:00 (24hr format)
  rtc.setDate(1, 1, 2014);   // Set the date to January 1st, 2014

  
  Serial.begin(9600);
  radio.stopListening();
}

void loop()
{
  int piezoADC = analogRead(PIEZO_PIN);
  strcpy(myData.dateAndTime, returnDateAndTime(dateAndTime));
  myData.tempVal1 = getTemperature(temperatureFirst);
  myData.tempVal2 = getTemperature(temperatureSecond);
  //radio.write(&myData, sizeof(myData));
  Serial.print("Datums un laiks: ");
  Serial.print(myData.dateAndTime);
  Serial.println(" ");
  Serial.print("Ja kustas atgriez 1 : ");
  Serial.print(moving(piezoADC));
  Serial.println(" ");
  Serial.print("Temperature1: ");
  Serial.print( myData.tempVal1);
  Serial.println(" ");
  Serial.print("Temperature2: ");
  Serial.println(myData.tempVal2);
  Serial.println("=================================");

  delay(1000);
}

char* returnDateAndTime(char dateAndTime[20]) {

  char* date = rtc.getDateStr();
  char* time = rtc.getTimeStr();

  strcpy(dateAndTime, date);
  strcat(dateAndTime, "/");
  strcat(dateAndTime, time);

  return dateAndTime;
}

float getTemperature(OneWire sensor) {
  byte present = 0;
  byte data[12];
  byte address[8];
  float celsius;

//  if ( !sensor.search(address)) {
//    sensor.reset_search();
//    delay(250);
//    return;
//  }

  sensor.reset();
  sensor.select(address);
  sensor.write(0x44);

  present = sensor.reset();
  sensor.select(address);    
  sensor.write(0xBE);

  for (byte i = 0; i < 9; i++) {
    data[i] = sensor.read();
  }

  int16_t rawTemperatureValue = (data[1] << 8) | data[0];
  celsius = (float)rawTemperatureValue / 16.0;
  return celsius;
}

int moving(int piezoADC) {
   float piezoV = piezoADC / 1023.0 * 5.0;

   if (piezoV > 0) {
     return 1;
   } else{
     return 0;
   }
}
