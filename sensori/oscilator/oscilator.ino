//http://www.instructables.com/id/Real-time-clock-using-DS3231-EASY/step2/Library/

#include <DS3231.h>
#include <String.h>

// Init the DS3231 using the hardware interface
DS3231  rtc(SDA, SCL);

void setup()
{
  Serial.begin(9600);
  rtc.begin();
  
  // The following lines can be uncommented to set the date and time
  //rtc.setDOW(WEDNESDAY);     // Set Day-of-Week to SUNDAY
  //rtc.setTime(12, 0, 0);     // Set the time to 12:00:00 (24hr format)
  //rtc.setDate(1, 1, 2014);   // Set the date to January 1st, 2014
}

void loop()
{
  char dateAndTime[30];

  char* date = rtc.getDateStr();
  char* time = rtc.getTimeStr();

  strcpy(dateAndTime, date);
  strcat(dateAndTime, "/");
  strcat(dateAndTime, time);
  

  Serial.println(dateAndTime);
 
  delay (1000);
}
