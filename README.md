# Ligzdojošo putnu ligzdu monitorēšanas senosoru mezgls #

## Projektu izstrādāja ##
* Elvijs Ernests Viļķels : ev14027
* Raivis Ginters : rg14013
* Kristaps Legzdiņš : kl14023

## Mikrokontrolieri ##
* [Arduino Pro Mini](http://reinholds.zviedris.lv/wiki/_detail/projects/promini.png?id=projects%3Aarduino) Chinese version
* [Arduino Deumilanova](https://www.arduino.cc/en/Main/ArduinoBoardDuemilanove)

## Sensoru resursi ##
* [NRF 24L01 2.4Ghz radio raidītājs](https://arduino-info.wikispaces.com/Nrf24L01-2.4GHz-HowTo#lib)
* [Temperatūras sensors](http://www.tweaking4all.com/hardware/arduino/arduino-ds18b20-temperature-sensor/)
* [Vibrāciju sensors](https://learn.sparkfun.com/tutorials/piezo-vibration-sensor-hookup-guide)
* [LCD](https://www.arduino.cc/en/Tutorial/HelloWorld?from=Tutorial.LiquidCrystal)
* [Reālā laika pulkstenis](http://howtomechatronics.com/tutorials/arduino/arduino-ds3231-real-time-clock-tutorial)
* [3G GSM SIM800L datu raidītājs](http://www.electrodragon.com/product/sim800l-mini-dev-board-gsmgprs/#prettyPhoto)

## iOS / Android lietotne ##
* [BlynkApp](http://blynk.cc)

# Apraksts #
Bezvadu sensoru tīklu kursa ietvaros mūsu grupai bija uzdevums izveidot sensoru tīklu ar vismaz vienu sensoru mezglu ,kas monitorēs putnu ligzdas. Šis projekts ir īstenots sadarbībā ar Latvijas Ornitoloģijas Biedrību. Izmantojot šāda veida risinājumu visnotaļ uzlabotu šī procesa izpildi, ja ornitologam nebūtu nepieciešamība kāpt koka galotnē, lai noteiktu kāds stāvoklis ir putna ligzdā. Papildus tas arī netraucēs putnam ,atrodoties savā ligzdā. Sensoru mezgls varēs noteikt vai ligzdā mitinās kāds putns vai arī tā ir tukša. 

# Projekta izstrāde #
Lai izveidotu šāda veida sensoru tīklu mēs izmantojām 2 Arduino mikrokontrolierus un augstāk minētos sensorus, kā arī moduļus. Ļoti svarīgi ir pareizi izvēlēties attiecīgos sensorus ,paredzot kāda būs to nepieciešamība mezglā. 
Bez sensoru noteiktajiem mērījumiem sensoru tīklam svarīga ir saziņas maršrutizācija un datu pārraide savstarpēji starp mezgliem. Tas tika risināts izmantojot datu pārraides moduļus - NRF 2.4 GHz radio frekvenču īsa attāluma pārraides modulis un 3G GSM SIM datu pārraides modulis, kas datus var pārraidīt izmantojot mobilo tīklāju.
Sensorus un moduļus ir arī jānoprogrammē ,lai tie korekti pildītu savas paredzētās funkcijas, kā arī apstrādātu iegūtos datus, lai pārsūtītu tālāk. Programmēšana tika veikta uz Arduino izstrādes vides ar C/C++ valodas iezīmēm.

## Datu savākšanas cikls ##
Datu savākšanas cikls, kas ir implementēts sensoru tīklā ir redzama diagrammā un ir sekojoša: 
1. Ornitologs izmantojot mobilo ierīci ar uzstādītu Blynk lietotni nosūta pieprasījumu uz sensoru tīkla bāzes staciju ,kas novietota starp galvenajiem sensoru mezgliem putnu ligzdās. Bāzes stacija atrodas aptuveni 3-5 metrus no zemes, lai meža zvēri un neautorizēti cilvēki tai nevarētu piekļūt. 
2. Pēc pieprasījuma saņemšanas bāzes stacijā, ir norādīts no kura sensora mezgla nepieciešams nolasīt datus (Iespējams paredzēt arī tādu gadījumu, kur visiem sensoru mezgliem ligzdās dati tiek nolasīti uzreiz secībā, un tiek glabāti uz bāzes stacijas), tad bāzes stacija "pamodina" no guļus stāvokļa atrodošos sensora mezglu 
3. Sensora mezgls aktivizējies mērījumu veikšanai tos veic un nolasītos datus pārsūta bāzes stacijai. Sensora mezgls ligzdā veic minimālu datu apstrādi, drīzāk datu korektu nolasīšanu un formatēšanu, lai uz bāzes staciju tiktu nosūtīti jau cilvēkam saprotami dati.
4. Bāzes stacijā tālāk dati tiks apstrādāti tā ,lai varētu veikt secinājumus par ligzdā esošo situāciju.

## Diagramma ##

![diagramma.png](https://bitbucket.org/repo/d4EdEb/images/3170466126-diagramma.png)